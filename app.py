from flask import Flask, render_template

# Create flask instance
app = Flask(__name__)

# Create a route decorator
@app.route('/')
@app.route('/home')
# def index():
#     return "<h1>Hello World!</h1>"

def index():
    return render_template("index.html")


@app.route('/user/<name>')
# def user(name):
#     return f"<h1>Hello {name}</h1>"

def user(name):
    return render_template('user.html', name=name)

if __name__ == "__main__":
    app.run(debug=True, port=9000)